'use strict';

module.exports = (sequelize, DataTypes) => {
	const Game = sequelize.define('Game', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
	});
	Game.associate = function(models) {
		models.Game.hasMany(models.Player, { foreignKey: 'game_id', targetKey: 'id'});
	};

	Game.sync();

	return Game;
};
