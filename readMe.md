## Install

run `npm i`

## Run server

run `node server.js`

Server will run on port 8080

## Config

Port on ./config.js

Bdd on ./config/config.js

Change access for bdd

## Architecture

./config
Config for BDD

./controllers
All controllers for entities

./models
All models for entities

./routes
All routes for API

./config.js
Config for server, port

./server.js
Server nodejs with config and run