const models = require('../models');
const Sequelize = require('sequelize');

function index(req, res) {
    try {
        models.Game.findAll({ 
            include : {
                model:models.Player
            }
        }).then(
            games => res.send(games)
        );
    }
    catch (error) {
        console.error(error);
    }
}

function create(req, res) {
    try {

        models.Game.create(req.body).then(
            game => res.send(game)
        );
        
    }
    catch (error) {
        console.error(error);
    }
}

function show(req, res) {
    try {
        const id = req.params.id;
        models.Game.findOne(
            { where: { id: id }, 
            include : {
                model:models.Player
            } 
        }).then((game) => {
            res.send(game);
        });
    }
    catch (error) {
        console.error(error);
    }
}

function update(req, res) {
    try {
        const id = req.params.id;
        models.Game.findOne({ where: { id: id } }).then((game) => {
            game.update(req.body);
            res.send(game);
        });
    }
    catch (error) {
        console.error(error);
    }
}

function destroy(req, res) {
    try {
        const id = req.params.id;
        models.Game.findOne({ where: { id: id } }).then((game) => {
            game.destroy();
            res.send({'message': 'Game ' + game.id + ' is destroy'});
        });
    }
    catch (error) {
        console.error(error);
    }
}


function findGame(req, res) {
    try {
        models.Game.findAll({
            include: [
                {
                    model: models.Player,
                    required: true,
                }
            ],
            group: 'Game.id',
            having: Sequelize.where(Sequelize.fn('COUNT', Sequelize.col('Players.id')), '=', 1)
        })
        .then((game) => {
            res.send(game);
        });
    }
    catch (error) {
        res.send(error);
    }
}

module.exports = {
    index: index,
    create: create,
    show: show,
    update: update,
    destroy: destroy,
    findGame: findGame
}
